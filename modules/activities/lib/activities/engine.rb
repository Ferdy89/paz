module Activities
  class Engine < ::Rails::Engine
    isolate_namespace Activities
    config.generators.test_framework = :rspec
  end
end
