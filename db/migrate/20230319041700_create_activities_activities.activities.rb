# This migration comes from activities (originally 20230319035009)
class CreateActivitiesActivities < ActiveRecord::Migration[7.0]
  def change
    create_table :activities_activities do |t|
      t.string :name
      t.text :description
      t.integer :user_id

      t.timestamps
    end
  end
end
