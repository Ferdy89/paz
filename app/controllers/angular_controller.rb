class AngularController < ApplicationController

  def app
    path = if Rails.public_path.join(params[:path]).exist?
      # The asset exists (js, css, ...), we want to serve it
      params[:path]
    else
      # The path requested must be meant for the Angular router then, so we
      # serve the Angular app and let it handle it
      "index.html"
    end

    send_file Rails.public_path.join(path), disposition: 'inline'
  end
end
