import {
  ChangeDetectionStrategy,
  Component,
  Input,
  NgModule,
  OnInit,
} from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { Subject, map } from 'rxjs';

import { Activity } from './activity.model';

@Component({
  selector: 'activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivityComponent implements OnInit {
  @Input() activity!: Activity;

  @Input() completed$?: Subject<Activity>;

  protected readonly completedControl = new FormControl<boolean>(false, {
    nonNullable: true,
  });

  constructor() {}

  ngOnInit(): void {
    if (this.activity === undefined) {
      throw new Error('The Activity component requires an [activity] input');
    }
    this.completedControl.setValue(!!this.activity.isCompleted);

    if (this.completed$) {
      this.completedControl.valueChanges
        .pipe(
          map((completed: boolean) => {
            const activity: Activity = {
              ...this.activity, // TODO: Maybe deep-clone? Or mutate the @Input?
              isCompleted: completed,
            };
            return activity;
          })
        )
        .subscribe(this.completed$);
    }
  }
}

@NgModule({
  declarations: [ActivityComponent],
  exports: [ActivityComponent],
  imports: [MatCheckboxModule, ReactiveFormsModule],
})
export class ActivityModule {}
