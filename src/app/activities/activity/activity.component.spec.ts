import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReplaySubject, Subject, firstValueFrom } from 'rxjs';

import { ActivityModule } from './activity.component';
import { ActivityHarness } from './activity.harness';
import { Activity } from './activity.model';

interface TestInputs {
  activity: Activity;
  completed$?: Subject<Activity>;
}

async function initializeTest(inputs: TestInputs) {
  await TestBed.configureTestingModule({
    declarations: [TestHostComponent],
    imports: [ActivityModule],
  }).compileComponents();
  const fixture: ComponentFixture<TestHostComponent> =
    TestBed.createComponent(TestHostComponent);

  const component: TestHostComponent = fixture.componentInstance;
  component.activity = inputs.activity;
  component.completed$ = inputs.completed$;
  fixture.detectChanges();

  const loader: HarnessLoader = TestbedHarnessEnvironment.loader(fixture);
  const harness: ActivityHarness = await loader.getHarness(ActivityHarness);

  return {
    harness,
  };
}

describe('ActivityComponent', () => {
  it('displays the name', async () => {
    const { harness } = await initializeTest({
      activity: {
        name: 'My first activity!',
      },
    });

    expect(await harness.name()).toEqual('My first activity!');
  });

  it('displays the description', async () => {
    const { harness } = await initializeTest({
      activity: {
        name: 'My activity',
        description: 'This is so cool!',
      },
    });

    expect(await harness.description()).toEqual('This is so cool!');
  });

  it('is not completed by default', async () => {
    const { harness } = await initializeTest({
      activity: {
        name: 'My activity',
      },
    });

    expect(await harness.isCompleted()).toBe(false);
  });

  it("shows when it's completed", async () => {
    const { harness } = await initializeTest({
      activity: {
        name: 'My activity',
        isCompleted: true,
      },
    });

    expect(await harness.isCompleted()).toBe(true);
  });

  it('can be marked completed', async () => {
    const { harness } = await initializeTest({
      activity: {
        name: 'My activity',
      },
    });

    await harness.complete();

    expect(await harness.isCompleted()).toBe(true);
  });

  it("emits when it's completed", async () => {
    const activity: Activity = {
      name: 'My activity',
    };
    const completed$ = new ReplaySubject<Activity>(1);
    const { harness } = await initializeTest({ activity, completed$ });

    await harness.complete();

    const completed = await firstValueFrom(completed$);
    expect(completed).toEqual({
      ...activity,
      isCompleted: true,
    });
  });
});

@Component({
  selector: 'host-component',
  template: `<activity
    [activity]="activity"
    [completed$]="completed$"
  ></activity>`,
})
class TestHostComponent {
  activity?: Activity;
  completed$?: Subject<Activity>;
}
