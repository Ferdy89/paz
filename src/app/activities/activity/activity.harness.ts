import {
  AsyncFactoryFn,
  ComponentHarness,
  TestElement,
} from '@angular/cdk/testing';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';

export class ActivityHarness extends ComponentHarness {
  static hostSelector = 'activity';

  async name(): Promise<string> {
    const element: TestElement = await this.locatorFor('span.name')();

    return await element.text();
  }

  async description(): Promise<string> {
    const element: TestElement = await this.locatorFor('span.description')();

    return await element.text();
  }

  async complete(): Promise<void> {
    return (await this.getCompleteElement()).check();
  }

  async isCompleted(): Promise<boolean> {
    return (await this.getCompleteElement()).isChecked();
  }

  private async getCompleteElement(): Promise<MatCheckboxHarness> {
    return this.locatorFor(MatCheckboxHarness.with({ label: 'Completed' }))();
  }
}
