export interface Activity {
  name: string;
  description?: string;
  isCompleted?: boolean;
}
