import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { ReplaySubject, firstValueFrom } from 'rxjs';

import { Activity } from './activity/activity.model';
import { ApiService } from './api.service';

describe('ApiService', () => {
  let service: ApiService;
  let mockHttp: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(ApiService);
    mockHttp = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    mockHttp.verify();
  });

  describe('#listActivities', () => {
    it('fetches a list of Activities from the backend', async () => {
      const backendActivities = [
        {
          name: 'Write some Paz code',
          description: 'I really like coding, I should make some time for Paz!',
          isCompleted: true,
        },
        { name: 'Groceries' },
      ];
      const receivedActivities$ = new ReplaySubject<Activity[]>();

      service.listActivities().subscribe(receivedActivities$);

      const req = mockHttp.expectOne({ url: '/api/activities', method: 'GET' });
      req.flush(backendActivities);
      expect(await firstValueFrom(receivedActivities$)).toEqual([
        {
          name: 'Write some Paz code',
          description: 'I really like coding, I should make some time for Paz!',
          isCompleted: true,
        },
        { name: 'Groceries' },
      ]);
    });
  });

  describe('#createActivity', () => {
    it('posts a request to the backend', () => {
      // The names are different to make the test more explicit, but in real
      // life the backend would create an Activity with the same properties as
      // the Activity that was sent from the client
      const newActivity: Activity = {
        name: 'Creating my first activity',
      };
      const backendActivity: Activity = {
        name: 'First activity created!',
      };

      service.createActivity(newActivity).subscribe();

      const req = mockHttp.expectOne({
        url: '/api/activities',
        method: 'POST',
      });
      expect(req.request.body).toEqual(newActivity);
      req.flush(backendActivity);
    });

    it('returns the created activity', async () => {
      // The names are different to make the test more explicit, but in real
      // life the backend would create an Activity with the same properties as
      // the Activity that was sent from the client
      const newActivity: Activity = {
        name: 'Creating my first activity',
      };
      const backendActivity: Activity = {
        name: 'First activity created!',
      };
      const receivedActivity$ = new ReplaySubject<Activity>();

      service.createActivity(newActivity).subscribe(receivedActivity$);

      const req = mockHttp.expectOne({
        url: '/api/activities',
        method: 'POST',
      });
      expect(req.request.body).toEqual(newActivity);
      req.flush(backendActivity);
      expect(await firstValueFrom(receivedActivity$)).toEqual(backendActivity);
    });
  });
});
