import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Activity } from './activity/activity.model';

@Injectable({ providedIn: 'root' })
export class ApiService {
  constructor(private readonly httpClient: HttpClient) {}

  listActivities(): Observable<Activity[]> {
    return this.httpClient
      .get('/api/activities')
      .pipe(map((response) => response as Activity[]));
  }

  createActivity(activity: Activity): Observable<Activity> {
    return this.httpClient
      .post('/api/activities', activity)
      .pipe(map((response) => response as Activity));
  }
}
