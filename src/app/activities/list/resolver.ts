import {
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  ResolveFn,
} from '@angular/router';
import { Activity } from '../activity/activity.model';
import { ApiService } from '../api.service';
import { inject } from '@angular/core';
import { Observable } from 'rxjs';

export const activityListResolver: ResolveFn<Activity[]> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot,
  activityService: ApiService = inject(ApiService)
) => activityService.listActivities();
