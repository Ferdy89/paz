import {
  ChangeDetectionStrategy,
  Component,
  Input,
  NgModule,
  OnInit,
} from '@angular/core';
import { Activity } from '../activity/activity.model';
import { ActivityModule } from '../activity/activity.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'activity-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivityListComponent implements OnInit {
  @Input() activities!: Activity[];

  constructor() {}

  ngOnInit(): void {
    if (this.activities === undefined) {
      throw new Error(
        'The ActivityList component requires the [activities] input'
      );
    }
  }
}

@NgModule({
  declarations: [ActivityListComponent],
  exports: [ActivityListComponent],
  imports: [ActivityModule, CommonModule],
})
export class ActivityListModule {}
