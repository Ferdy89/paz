import { TestBed } from '@angular/core/testing';

import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, firstValueFrom, of } from 'rxjs';
import { ApiService } from '../api.service';
import { Activity } from '../activity/activity.model';
import { activityListResolver } from './resolver';

interface TestInputs {
  activities: Activity[];
}

async function initializeTest(inputs: TestInputs) {
  const activityService = jasmine.createSpyObj<ApiService>({
    listActivities: of(inputs.activities),
  });
  TestBed.configureTestingModule({
    providers: [{ provide: ApiService, useValue: activityService }],
  });
}

describe('ActivityListResolver', () => {
  it('fetches the list of activities', async () => {
    await initializeTest({
      activities: [
        {
          name: 'My first activity!',
        },
      ],
    });
    const route: ActivatedRouteSnapshot = new ActivatedRouteSnapshot();
    const state: RouterStateSnapshot = {
      url: 'irrelevantUrl',
      root: route,
    };

    TestBed.runInInjectionContext(async () => {
      const activities$ = activityListResolver(route, state) as Observable<
        Activity[]
      >;

      expect(await firstValueFrom(activities$)).toEqual([
        {
          name: 'My first activity!',
        },
      ]);
    });
  });
});
