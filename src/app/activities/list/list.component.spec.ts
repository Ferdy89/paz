import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityListModule } from './list.component';
import { Activity } from '../activity/activity.model';
import { Component } from '@angular/core';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ListActivitiesHarness } from './list.harness';

interface TestInputs {
  activities: Activity[];
}

async function initializeTest(inputs: TestInputs) {
  await TestBed.configureTestingModule({
    declarations: [TestHostComponent],
    imports: [ActivityListModule],
  }).compileComponents();
  const fixture: ComponentFixture<TestHostComponent> =
    TestBed.createComponent(TestHostComponent);

  const component: TestHostComponent = fixture.componentInstance;
  component.activities = inputs.activities;
  fixture.detectChanges();

  const loader: HarnessLoader = TestbedHarnessEnvironment.loader(fixture);
  const harness: ListActivitiesHarness = await loader.getHarness(
    ListActivitiesHarness
  );

  return {
    harness,
  };
}

describe('ListActivitiesComponent', () => {
  it('displays a list of activities', async () => {
    const { harness } = await initializeTest({
      activities: [
        {
          name: 'My first activity!',
        },
        {
          name: 'My second one!',
        },
      ],
    });

    const activityNames = await Promise.all(
      (await harness.activities()).map((activity) => activity.name())
    );
    expect(activityNames.sort()).toEqual(
      ['My first activity!', 'My second one!'].sort()
    );
  });
});

@Component({
  selector: 'host-component',
  template: `<activity-list [activities]="activities"></activity-list>`,
})
class TestHostComponent {
  activities!: Activity[];
}
