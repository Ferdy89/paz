import { ComponentHarness } from '@angular/cdk/testing';
import { ActivityHarness } from '../activity/activity.harness';

export class ListActivitiesHarness extends ComponentHarness {
  static hostSelector = 'activity-list';

  async activities(): Promise<ActivityHarness[]> {
    return this.locatorForAll(ActivityHarness)();
  }
}
