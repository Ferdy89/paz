import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';

import {
  ListActivitiesComponent,
  ListActivitiesModule,
} from './list-activities.component';
import { Activity } from 'src/app/activities/activity/activity.model';
import { ListActivitiesHarness } from 'src/app/activities/list/list.harness';
import { of } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

interface TestInputs {
  activities: Activity[];
}

async function initializeTest(inputs: TestInputs) {
  const route: Partial<ActivatedRoute> = {
    data: of({
      activities: inputs.activities,
    }),
  };
  await TestBed.configureTestingModule({
    declarations: [ListActivitiesComponent],
    imports: [ListActivitiesModule],
    providers: [{ provide: ActivatedRoute, useValue: route }],
  }).compileComponents();

  const fixture: ComponentFixture<ListActivitiesComponent> =
    TestBed.createComponent(ListActivitiesComponent);

  fixture.detectChanges();

  const loader: HarnessLoader = TestbedHarnessEnvironment.loader(fixture);
  const harness: ListActivitiesHarness = await loader.getHarness(
    ListActivitiesHarness
  );

  return {
    harness,
  };
}

describe('ListActivitiesComponent', () => {
  it('displays a list of activities', async () => {
    const { harness } = await initializeTest({
      activities: [
        {
          name: 'My first activity!',
        },
        {
          name: 'My second one!',
        },
      ],
    });

    const activityNames = await Promise.all(
      (await harness.activities()).map((activity) => activity.name())
    );
    expect(activityNames.sort()).toEqual(
      ['My first activity!', 'My second one!'].sort()
    );
  });
});
