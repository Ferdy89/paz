import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  NgModule,
  OnInit,
  inject,
} from '@angular/core';
import { ActivatedRoute, Routes } from '@angular/router';
import { Observable, map } from 'rxjs';
import { Activity } from 'src/app/activities/activity/activity.model';
import { ActivityListModule } from 'src/app/activities/list/list.component';
import { activityListResolver } from 'src/app/activities/list/resolver';

@Component({
  templateUrl: './list-activities.component.html',
  styleUrls: ['./list-activities.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListActivitiesComponent implements OnInit {
  private route = inject(ActivatedRoute);

  protected activities$: Observable<Activity[]> = this.route.data.pipe(
    map((data) => {
      const activities: Activity[] = data['activities'] as Activity[];

      return activities;
    })
  );

  constructor() {}

  ngOnInit(): void {}
}

export const ROUTES: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ListActivitiesComponent,
    resolve: {
      activities: activityListResolver,
    },
  },
];

@NgModule({
  declarations: [ListActivitiesComponent],
  exports: [ListActivitiesComponent],
  imports: [ActivityListModule, CommonModule],
})
export class ListActivitiesModule {}
