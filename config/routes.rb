Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  scope "api" do
    mount Activities::Engine => "/"
  end

  get "(*path)" => "angular#app"
end
