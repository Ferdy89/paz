# Full documentation for Rails Application Templates
# https://guides.rubyonrails.org/rails_application_templates.html

# This template is meant to be used on engines. The gemspec generated doesn't
# work, so this removes the TODOs and URL fields to make it valid. It's
# irrelevant for now, anyway
inside('.') do # Fixes incorrect spec/dummy path otherwise
  run "sed -i 's/TODO/DONE/g' #{@name}.gemspec"
  run "sed -i '/_uri/d' #{@name}.gemspec"
  run "sed -i '/homepage/d' #{@name}.gemspec"
end

gem_group :development, :test do
  gem 'rspec-rails', '~> 6.0.0'
end
rails_command 'generate rspec:install'

inside('.') do # Fixes incorrect spec/dummy path otherwise
  run 'BUNDLE_GEMFILE=Gemfile bundle binstub bundler'
  run 'BUNDLE_GEMFILE=Gemfile bundle binstub rspec-core'
end

# TODO: automate the rest of the advice from
# https://www.hocnest.com/blog/testing-an-engine-with-rspec/
# sed spec/rails_helper.rb
#  require_relative './dummy/config/environment'
#  ActiveRecord::Migrator.migrations_paths = File.join(ENGINE_ROOT, 'spec/dummy/db/migrate')
# sed lib/activities/engine.rb
#  config.generators.test_framework = :rspec
