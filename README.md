# Paz

Paz helps you organize your life and find your path.

## Development

The backend is Rails and the frontend is Angular.

### First time

It requires PostgreSQL installed on your system, with the `dev` package so the `pg` gem can compile (`sudo apt install libpq-dev` on Linux).

The recommended setup is to use `chruby` to manage the ruby version and `nvm` to
manage the Node version. There are `.rc` files for both.

Install the [Angular CLI](https://github.com/angular/angular-cli).

Run `bin/bundle install` and `npm install` to install the Ruby and Node
packages, respectively.

Run `bin/rails db:create` to initialize the database.

### Serve locally

Use `bin/dev` to build the Angular app and run a Rails server for it. Visit it
on http://localhost:3000

### Run tests

Run tests for the backend using `bin/rspec` and for the frontent using `npm run
test`.

### Create new engine

Run `bin/rails plugin new --rc=.railsrc modules/your-module-name`.

## General system structure

The idea is to keep it modularized through Rails Engines. In the future I dream
about making the boundaries across engines enforced through gRPCs and protos,
but it's too much hassle to begin with.
